class AddFieldsForVitaminAs < ActiveRecord::Migration[5.1]
  def change
    add_column :vitamin_as, :batch_number, :integer
    add_column :vitamin_as, :dose_number, :integer
  end
end
