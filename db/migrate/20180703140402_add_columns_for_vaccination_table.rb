class AddColumnsForVaccinationTable < ActiveRecord::Migration[5.1]
  def change
    add_column :vaccinations, :batch_number, :integer
    add_column :vaccinations, :dose_number, :integer 
  end
end
