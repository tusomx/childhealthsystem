class AddNewColumnsForBabies < ActiveRecord::Migration[5.1]
  def change
    add_column :babies, :duration_of_labour, :decimal, precision: 10, scale: 2,  default: 0.5
    add_column :babies, :agpr_score, :integer, default: 1
    add_column :babies, :mode_of_delivery, :string
    add_column :babies, :head_circumference, :decimal, precision: 10, scale: 2,  default: 0.0
  end
end
