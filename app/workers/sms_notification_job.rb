class SmsNotificationJob
  include Sidekiq::Worker

  def perform mother_id, message = ""
    mother = Mother.find mother_id
    
    NotifierMailer.reminder_for_vaccination(mother, message).deliver_now
    TwilioTextMessenger.new(mother.phone_number, message)
  end
end
