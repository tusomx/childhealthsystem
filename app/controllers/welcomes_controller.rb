class WelcomesController < ApplicationController

  def download_data
    respond_to do |format|
      format.html
      format.csv { send_data Baby.import, filename: "Babies-chart-#{Date.today}.csv"} 
    end
  end

  def new
    if params["date"].present?
      @babies = Baby.get_result params
    end
    
    baby_collection = Baby.all
    infant_babies = BabyInfantFeeding.all    

    @male_babies = baby_collection.male_babies.count
    @female_babies = baby_collection.female_babies.count
    @hospital_babies = baby_collection.joins(:hospital).select("hospitals.id, count(babies.id) as babies_count, hospitals.name as hospital_name").group("hospitals.id")

    @city_babies = baby_collection.group_by{|b| b.place_of_birth.downcase}.map {|k,v| OpenStruct.new({"city_name" => k, "size" => v.size})}
  
    @babies_by_delivery_method = baby_collection.select("count(babies.id) as babies_count, mode_of_delivery").group("mode_of_delivery")

    @high_agpr_score = baby_collection.where("agpr_score >= ? AND agpr_score < ?", 5, 11).count    
    @low_agpr_score = baby_collection.where("agpr_score >= ? AND agpr_score < ?", 0, 5).count
    @below_normal_weight = baby_collection.joins(:risk_factors).where("lower(risk_factors.title) = ?", "birth weight less than 2.5kgs").count
    #@below_normal_weight = baby_collection.joins(:healths).where("healths.weight < ?", 2.5).count
    
    @normal_weight = baby_collection.joins(:healths).where("healths.weight >= ?", 2.5).count
    @normal_height = baby_collection.joins(:healths).where("healths.height < ?", 46).count
    @overnormal_height = baby_collection.joins(:healths).where("healths.height >= ?", 46).count

    @hiv_mothers = baby_collection.joins(:risk_factors).merge(RiskFactor.mtct).count
    @hiv_babies = baby_collection.joins(:risk_factors).merge(RiskFactor.mtct).count
    @non_hiv_babies = baby_collection.joins(:risk_factors).where("lower(risk_factors.title) != ?", 'mtct').uniq.count

    
    @mtct_babies = baby_collection.joins(:risk_factors).where("lower(risk_factors.title) = ?", "mtct").count
    @fifth_child_or_more = baby_collection.joins(:risk_factors).where("lower(risk_factors.title) = ?", "fifth child or more").count
    @birth_interval = baby_collection.joins(:risk_factors).where("lower(risk_factors.title) = ?", "birth intervals less than 2 years").count
    @multiple_births = baby_collection.joins(:risk_factors).where("lower(risk_factors.title) = ?", "multiple births").count
    @death_of_any_child = baby_collection.joins(:risk_factors).where("lower(risk_factors.title) = ?", "death of any child under 5 in family").count
    @single_parent_babies = baby_collection.joins(:risk_factors).where("lower(risk_factors.title) = ?", "single parents").count
    @maternal_death_babies = baby_collection.joins(:risk_factors).where("lower(risk_factors.title) = ?", "maternal death").count
    @asphyxia_babies = baby_collection.joins(:risk_factors).where("lower(risk_factors.title) = ?", "severe asphyxia").count
    @severe_jaundice_babies = baby_collection.joins(:risk_factors).where("lower(risk_factors.title) = ?", "severe jaundice").count

    @fed_from_birth_to_6M = infant_babies.joins(:infant_feeding_label).where("infant_feeding_labels.min_duration >= ? AND infant_feeding_labels.max_duration <= ?", 1, 6).count

    @fed_from_birth_to_24M = infant_babies.joins(:infant_feeding_label).where("infant_feeding_labels.min_duration >= ? AND infant_feeding_labels.max_duration <= ?", 1, 24).count

    @star_diet = infant_babies.joins(:infant_feeding_label).where("lower(infant_feeding_labels.title) = ?", "at 6months, does baby compliment breastfeeding by eating 4 star diet").count
    
    @meals_per_day = infant_babies.joins(:infant_feeding_label).where("lower(infant_feeding_labels.title) = ?", "at 6months, does baby compliment breastfeeding by eating 4 meals per day").count

    @vitamin_babies = baby_collection.joins(:vitamin_as).where("vitamin_as.month_duration = ?", '1').count
  end
end