class NotifierMailer < ApplicationMailer
  
  def send_email(baby)
    @baby = baby
    @email = @baby.mother.try(:email)
    mail(to: @email, subject: "Child-Health-Care – Vaccinations Awareness for #{@baby.full_name}")
  end

  def reminder_for_vaccination mother, subject
    @mother = mother
    @email = @mother.try(:email)
    @subject = subject
    mail(to: @email, subject: subject)
  end

end