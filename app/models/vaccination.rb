require 'csv'
class Vaccination < ApplicationRecord
  has_many :baby_vaccinations, dependent: :destroy
  has_many :babies, through: :baby_vaccinations
  validates :age, presence: true
  validates :title, presence: true
  validates :batch_number, presence: true
  validates :dose_number, presence: true


  def self.import_vaccinations(vaccinations)
    CSV.generate do |csv|
      if vaccinations.present?
        vaccinations.each do |vaccination|
          csv << ["VaccinationId", vaccination.id]
          csv << ["Title", vaccination.title]
          csv << ["Age", vaccination.age]
          csv << ["BatchNumber", vaccination.batch_number]
          csv << ["DoseNumber", vaccination.dose_number]
          csv << ["Description", vaccination.description]
          csv << []
        end
      else
        csv << ["You don't have any Vaccinations"]
      end
    end
  end
end

