class Father < ApplicationRecord
  has_many :babies, inverse_of: :father

  def full_name
    [first_name, last_name].join(" ")
  end
end
