class RiskFactor < ApplicationRecord
  has_many :baby_risk_factors
  validates :title, presence: true

  scope :mtct, -> { where(title: 'MTCT') }
end
