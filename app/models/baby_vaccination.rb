class BabyVaccination < ApplicationRecord
  belongs_to :vaccination
  belongs_to :baby
  validates :date, presence: true
  validates :vaccination_id, presence: true

  VACCINATION_DOSE = ["dose 1" , "dose 2", "dose 3"]

  after_create :send_notification!

  protected
    def send_notification!
      date_s = (Date.strptime(self.date, "%d-%m-%Y") - 1.day rescue Date.today).noon

      if date_s > Date.today
        SmsNotificationJob.perform_at(date_s, self.baby.mother_id, "Reminder for baby vaccination on #{self.date}")
      end
    end
end
