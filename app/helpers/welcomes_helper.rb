module WelcomesHelper
  LABEL =  (0..10).collect {|x| x*6 }
      
  def babies_age_chart_data
    babies = Baby.get_data
    
    @data = {
      labels: LABEL,
        datasets: [
        {
          radius: 0,
          tension: 0.6,
          label: "Severe stunted height",
          border_color: "#FDFFDE",
          background_color: "rgba(0,0,0,0.0)",
          data: babies.severe_stunted_height 
          
        },
        {
          radius: 0,
          tension: 0.6,
          label: "Severe under weight",
          border_color: "#FFAEAE",
          background_color: "rgba(0,0,0,0.0)",
          data: babies.severe_under_weight
          
        },
        {
          radius: 0,
          tension: 0.6,
          label: "Moderate under weight",
          border_color: "#a6ad87",
          background_color: "rgba(0,0,0,0.0)",
          data: babies.moderate_under_weight
          
        },
        {
          radius: 0,
          tension: 0.6,
          label: "Normal weight",
          border_color: "#B1B2E3",
          background_color: "rgba(0,0,0,0.0)",
          data: babies.normal_weight
          
        },
        {
          radius: 0,
          tension: 0.6,
          label: "Possible overweight",
          border_color: "#D6CDC8",
          background_color: "rgba(0,0,0,0.0)",
          data: babies.possible_overweight
          
        },
        {
          radius: 0,
          tension: 0.6,
          label: "Weight for height",
          border_color: "#556668",
          background_color: "rgba(0,0,0,0.0)",
          data: babies.weight_for_height
          
        },
        {
          radius: 0,
          tension: 0.6,
          label: "Moderate stunted height",
          background_color: "rgba(0,0,0,0.0)",
          border_color: '#1AB394',
          data: babies.moderate_stunted_height
          
        },
        {
          radius: 0,
          tension: 0.6,
          label: "Normal",
          background_color: "rgba(0,0,0,0.0)",
          border_color: '#9CC3DA',
          data: babies.normal
        },
        {
          radius: 0,
          tension: 0.6,
          label: "Normal 2",
          background_color: "rgba(0,0,0,0.0)",
          border_color: '#B5B8CF',
          data: babies.normal_2 
        },
        {
          radius: 0,
          tension: 0.6,
          label: "Possible very tall",
          background_color: "rgba(0,0,0,0.0)",
          border_color: '#9fe3f2',
          data: babies.possible_very_tall
        }
      ]
    }

    
    aa = [babies.severe_stunted_height,babies.severe_under_weight,babies.moderate_under_weight,babies.normal_weight,babies.possible_overweight,babies.weight_for_height,babies.moderate_stunted_height,babies.normal,babies.normal_2,babies.possible_very_tall].map do |a|
    
      {
        type: "spline",
        dataPoints: a
     }
    end
    
    aa = [{type: "spline", dataPoints: [{x: 5, y: 10}]}]
    
    return to_javascript_string([{x: 5, y: 10}])
  end
end